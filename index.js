import Vue from 'vue'
import Manipulator from './Manipulator.vue'

export default {
  install (Vue, options) {
    const axios = options
    Vue.component('manipulator', Manipulator)
    Vue.prototype.$axios = axios
  }
}
